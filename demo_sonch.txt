1, Viết api nhận request với đầu vào như sau

{
    "tokenKey": "1601353776839FT19310RH6P1",
    "apiID": "restPayment",
    "mobile": "0145225630",
    "bankCode": "970445",
    "accountNo": "0001100014211002",
    "payDate": "20200929112923",
    "addtionalData": "",
    "debitAmount": 11200,
    "respCode": "00",
    "respDesc": "SUCCESS",
    "traceTransfer": "FT19310RH6P1",
    "messageType": "1",
    "checkSum": "40e670720b754324af3d3a0ff49b52fb",
    "orderCode": "FT19310RH6P1",
    "userName": "cntest001",
    "realAmount": "11200",
    "promotionCode": "",
    "addValue": "{\"payMethod\":\"01\",\"payMethodMMS\":1}"
}

Với tokenKey - Là duy nhất và không được trùng trong 1 ngày, qua 0h ngày hôm sau là có được trùng
apiID - không được trắng
mobile - Số điện thoại của khách hàng
bankCode - Mã ngân hàng của (mặc định = 970445)

payDate - thời gian thanh toán , check định dạng :  yyyyMMddHHmmss

debitAmount - Số tiền thanh toán 

respCode - Mã hạch toán của ngân hàng (00- thành công)

respDesc - Mô tả mã lỗi đi kèm
traceTransfer
messageType - Mặc định là 1
orderCode - mã đặt hàng (Không được trống)
realAmount - Số tiền sau khuyến mại (Số tiền <= debitAmount)
promotionCode - Mã voucher (Nếu giá trị debit và real khác nhau tham số này phải khác null, '', '  ')

addValue = "{\"payMethod\":\"01\",\"payMethodMMS\":1}" Cố định

Bước1: validate  cơ bản như mô tả 

Bước 2: Đẩy dữ liệu lên queue 

Bước 3: Viết Project consumer data, insert data vào database 

Bước 4: Sau khi insert db thành công , đẩy thông tin vào api sau (Với input như sau:

{
    "tokenKey": "1601353776839FT19310RH6P1",
    "apiID": "restPayment",
    "mobile": "0145225630",
    "bankCode": "970445",
    "accountNo": "0001100014211002",
    "payDate": "20200929112923",
    "addtionalData": "",
    "debitAmount": 11200,
    "respCode": "00",
    "respDesc": "SUCCESS",
    "traceTransfer": "FT19310RH6P1",
    "messageType": "1",
    "checkSum": "40e670720b754324af3d3a0ff49b52fb",
    "orderCode": "FT19310RH6P1",
    "userName": "cntest001",
    "realAmount": "11200",
    "promotionCode": "",
    "queueNameResponse": "queue.payment.qrcodeV2.restPayment",
    "addValue": "{\"payMethod\":\"01\",\"payMethodMMS\":1}"
}

https://api.foodbook.vn/ipos/ws/xpartner/callback/vnpay

Bước 5: Nhận phản hồi từ api của đối tác , Phản hồi lại api mà đẩy dữ liệu lên queue theo đúng giao dịch đó



B, Những thông tin cần thiết


3, Thông tin rabbitMQ, redis

- 
RABBITMQ_HOST_PORT=10.22.7.88:6789

RABBITMQ_HOST=10.22.7.88
RABBITMQ_PORT=6789
RABBITMQ_USERNAME=qrcode
RABBITMQ_PASSWORD=qrcode
RABBITMQ_HEARTBEAT=45
RABBITMQ_VIRTUALHOST=qrcode



REDIS_MASTERNAME=redismaster
REDIS_PASSWORD=testredis@123
REDIS_HOST_MASTER=10.22.7.111
REDIS_PORT_MASTER=26379
REDIS_HOST_SLAVE=10.22.7.112
REDIS_PORT_SLAVE=26379

Thông tin DB oracle

DATABASE_URL=jdbc:oracle:thin:@10.22.7.63:1521/GTGTTEST
DATABASE_USER=MMS
DATABASE_PASS=mms





4, Yêu cầu
- Kết nối tới DB dùng connection pool (UCP hoặc HikariPool)
- Kết nối tới redis cũng tạo connection pool
- Viết log4j2 đầy đủ cho ứng dụng
- Kết nối tới Rabbit dùng ChannelPool
