DROP DATABASE IF EXISTS `Demo`;
CREATE DATABASE `Demo`;
USE `Demo`;

DROP TABLE IF EXISTS `Receipt`;
CREATE TABLE `Receipt`(
	id 				SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    api_id			VARCHAR(50) NOT NULL,
    mobile			VARCHAR(50) NOT NULL,
    bank_code		VARCHAR(50) DEFAULT 970445,
    accountNo		VARCHAR(50),
    pay_date		VARCHAR(50),
    additional_data	VARCHAR(50),
    debit_amount	DOUBLE,
    respCode		VARCHAR(50),
    respDesc		VARCHAR(50),
    trace_transfer	VARCHAR(50),
    message_type	VARCHAR(50) DEFAULT 1,
    `check_sum`		VARCHAR(50),
    order_code		VARCHAR(50) NOT NULL,
    user_name		VARCHAR(50) NOT NULL,
    real_amount		DOUBLE,
    promotion_code	VARCHAR(50),
    add_value		VARCHAR(50),
    create_date		DATETIME DEFAULT NOW() 
);